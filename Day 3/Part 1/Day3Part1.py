with open("../Part 2/Day3Part2.txt", "r") as f:
    input: list[str] = f.readlines()
    samechars: str = ""
    sumSame: int = 0
    found: bool = False
    foundChar: str = ""
    for line in input:
        # strips line of spaces and new line chars which was causing issues
        line = line.strip(" ").strip("\n")
        # splits line into two even lines
        first, second = line[:len(line)//2], line[len(line)//2 if len(line)%2 == 0 else ((len(line)+1)//2):]
        for char in first:
            if char in second:
                # Checks if a duplicate was found, avoids adding it to the end string
                if found and foundChar == char:
                    print("Duplicate found")
                # Trust no one
                elif found and foundChar != char:
                    print("Different Dupe found!")
                else:
                    samechars += char
                    foundChar = char
                    found = True
        # Troubleshooting code after not striping spaces and new line chars, good for future use
        if not found:
            print("No match found in", line, "part 1", first, "part 2", second)
        found = False
        foundChar = ""
    for match in samechars:
        # Checks if character's ascii value is 90 ('Z') or less
        if ord(match) < 91:
            sumSame += (ord(match) - 64) + 26
        # Checks if character's ascii value is 97 ('a') or greater
        elif ord(match) > 96:
            sumSame += (ord(match) - 96)
    print(sumSame)