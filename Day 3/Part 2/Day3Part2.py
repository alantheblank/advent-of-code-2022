with open("../Part 2/Day3Part2.txt", "r") as f:
    input: list[str] = f.readlines()
    samechars: str = ""
    sumSame: int = 0
    found: bool = False
    foundChar: str = ""
    for x in range(0, len(input), 3):
        pack: list[str] = [input[x].strip(" ").strip("\n"), input[x+1].strip(" ").strip("\n"), input[x+2].strip(" ").strip("\n")]
        for char in pack[0]:
            if (char in pack[1]) and (char in pack[2]):
                if found and foundChar == char:
                    print("Duplicate Found")
                elif found and foundChar != char:
                    print("Different Duplicate Found", "Found", foundChar, "new", char)
                    print(pack)
                else:
                    found = True
                    foundChar = char
                    samechars += char
        found = False
        foundChar = ""
    for match in samechars:
        # Checks if character's ascii value is 90 ('Z') or less
        if ord(match) < 91:
            sumSame += (ord(match) - 64) + 26
        # Checks if character's ascii value is 97 ('a') or greater
        elif ord(match) > 96:
            sumSame += (ord(match) - 96)
    print(sumSame)