with open("Day4Part2.txt", "r") as f:
    overlap: int = 0
    for line in f.readlines():
        # Load our job pair, strip it of any extra text, then break it down into numbers
        # pair one = 7-50, 8-33
        strippedLine: str = line.strip(" ").strip("\n")
        job1, job2 = strippedLine.split(",")
        j1num1, j1num2 = job1.split("-")
        j2num1, j2num2 = job2.split("-")
        # load the job areas into a list
        j1range = [x for x in range(int(j1num1), int(j1num2) + 1)]
        j2range = [x for x in range(int(j2num1), int(j2num2) + 1)]
        # check to see if any of the areas in the first job lot matches the areas in the second
        for num in j1range:
            if num in j2range:
                overlap += 1
                break
    print(overlap)