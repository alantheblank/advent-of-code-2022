
with open("Day4Part1.txt", "r") as f:
    redundantJobs: int = 0
    for line in f.readlines():
        # Load our job pair, strip it of any extra text, then break it down into numbers
        # pair one = 7-50, 8-33
        strippedLine: str = line.strip(" ").strip("\n")
        job1, job2 = strippedLine.split(",")
        j1num1, j1num2 = job1.split("-")
        j2num1, j2num2 = job2.split("-")
        try:
            # if the first number in the first job is smaller (8 > 7)
            if int(j1num1) < int(j2num1):
                # if the second number in the first job is bigger (50 > 30)
                if int(j1num2) >= int(j2num2):
                    redundantJobs += 1
                    print(job2, "is cntained in", job1)
            # A repeat but for the second job
            elif int(j1num1) > int(j2num1):
                if int(j1num2) <= int(j2num2):
                    redundantJobs += 1
                    print(job1, "is contained in", job2)
            # If the start number is the same we can assume it is contained
            else:
                redundantJobs += 1
                print("Start number is the same therefore contained")
        except ValueError:
            print("String", job1, job2, "failed to cast")
    print(redundantJobs)