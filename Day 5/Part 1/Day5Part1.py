with open("Day5Part1.txt", "r") as f:
    # Probably more efficient way of doing this
    stack1: list[str] = ["D", "L", "V", "T", "M", "H", "F"]
    stack2: list[str] = ["H", "Q", "G", "J", "C", "T", "N", "P"]
    stack3: list[str] = ["R", "S", "D", "M", "P", "H"]
    stack4: list[str] = ["L", "B", "V", "F"]
    stack5: list[str] = ["N", "H", "G", "L", "Q"]
    stack6: list[str] = ["W", "B", "D", "G", "R", "M", "P"]
    stack7: list[str] = ["G", "M", "N", "R", "C", "H", "L", "Q"]
    stack8: list[str] = ["C", "L", "W"]
    stack9: list[str] = ["R", "D", "L", "Q", "J", "Z", "M", "T"]
    stacks: list[list[str]] = [stack1, stack2, stack3, stack4, stack5, stack6, stack7, stack8, stack9]
    for line in f.readlines():
        # Start is given, check for when we're at instructions
        if "m" in line[0]:
            words = line.strip(" ").strip("\n").split(" ")
            command = int(words[1])
            source = int(words[3])
            dest = int(words[5])
            print(words)
            match source:
                case 1:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 2:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 3:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 4:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 5:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 6:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 7:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 8:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]
                case 9:
                    for x in range(0, command):
                        stacks[dest - 1].append(stacks[source - 1][len(stacks[source - 1]) - 1])
                        del stacks[source - 1][-1]

    for stack in stacks:
        print(stack)
