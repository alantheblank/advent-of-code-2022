with open("Day5Part2.txt", "r") as f:
    # Probably more efficient way of doing this
    stack1: list[str] = ["D", "L", "V", "T", "M", "H", "F"]
    stack2: list[str] = ["H", "Q", "G", "J", "C", "T", "N", "P"]
    stack3: list[str] = ["R", "S", "D", "M", "P", "H"]
    stack4: list[str] = ["L", "B", "V", "F"]
    stack5: list[str] = ["N", "H", "G", "L", "Q"]
    stack6: list[str] = ["W", "B", "D", "G", "R", "M", "P"]
    stack7: list[str] = ["G", "M", "N", "R", "C", "H", "L", "Q"]
    stack8: list[str] = ["C", "L", "W"]
    stack9: list[str] = ["R", "D", "L", "Q", "J", "Z", "M", "T"]
    stacks: list[list[str]] = [stack1, stack2, stack3, stack4, stack5, stack6, stack7, stack8, stack9]
    for line in f.readlines():
        # Start is given, check for when we're at instructions
        if "m" in line[0]:
            words = line.strip(" ").strip("\n").split(" ")
            command = int(words[1])
            source = int(words[3])
            dest = int(words[5])
            print(words)
            match source:
                case 1:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 2:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 3:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 4:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 5:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 6:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 7:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 8:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]
                case 9:
                    for char in stacks[source - 1][len(stacks[source - 1]) - command:]:
                        stacks[dest - 1].append(char)
                    del stacks[source - 1][len(stacks[source - 1]) - command:]

    for stack in stacks:
        print(stack)
